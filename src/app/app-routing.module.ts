import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { RepositoryCommitsComponent } from "./github-users/repository-commits/repository-commits.component";
import { RepositoryBranchesComponent } from "./github-users/repository-branches/repository-branches.component";
import { UserRepositoriesComponent } from "./github-users/user-repositories/user-repositories.component";

const routes: Routes = [
  { path: "", redirectTo: "/userRepositories", pathMatch: "full" },
  { path: "userRepositories", component: UserRepositoriesComponent },
  {
    path: "repositoryCommits/:owner/:repo/:sha",
    component: RepositoryCommitsComponent
  },
  {
    path: "repositoryCommits/:owner/:repo",
    component: RepositoryCommitsComponent
  },
  {
    path: "repositoryBranches/:owner/:repo",
    component: RepositoryBranchesComponent
  }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
