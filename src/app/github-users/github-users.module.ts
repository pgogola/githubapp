import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { UserRepositoriesComponent } from "./user-repositories/user-repositories.component";
import { HttpClientModule } from "@angular/common/http";
import { MatTabsModule } from "@angular/material/tabs";
import { MatButtonModule } from "@angular/material/button";
import { MatInputModule } from "@angular/material/input";
import { MatCardModule } from "@angular/material/card";
import { MatGridListModule } from "@angular/material/grid-list";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatTableModule } from "@angular/material/table";
import { MatDividerModule } from "@angular/material/divider";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatRadioModule } from "@angular/material/radio";
import { MatChipsModule } from "@angular/material/chips";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatNativeDateModule, MatRippleModule } from "@angular/material/core";
import { UserBasicInfoComponent } from "./user-basic-info/user-basic-info.component";
import { UserRepositoriesInfoComponent } from "./user-repositories-info/user-repositories-info.component";
import { RepositoryCommitsComponent } from "./repository-commits/repository-commits.component";
import { GithubRoutingModule } from "./github-routing.module";
import { RepositoryBranchesComponent } from "./repository-branches/repository-branches.component";

@NgModule({
  declarations: [
    UserRepositoriesComponent,
    UserBasicInfoComponent,
    UserRepositoriesInfoComponent,
    RepositoryCommitsComponent,
    RepositoryBranchesComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    MatTabsModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatGridListModule,
    MatExpansionModule,
    MatTableModule,
    MatSidenavModule,
    MatRadioModule,
    MatDividerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRippleModule,
    MatChipsModule,
    GithubRoutingModule
  ],
  exports: [UserRepositoriesComponent]
})
export class GithubUsersModule {}
