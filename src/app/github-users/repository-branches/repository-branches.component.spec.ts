import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepositoryBranchesComponent } from './repository-branches.component';

describe('RepositoryBranchesComponent', () => {
  let component: RepositoryBranchesComponent;
  let fixture: ComponentFixture<RepositoryBranchesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepositoryBranchesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepositoryBranchesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
