import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { UserRepositoriesManagerService } from "../user-repositories-manager.service";
import { Branch } from "../branch";
import { Commit } from "../commit";
import { Location } from "@angular/common";

@Component({
  selector: "app-repository-branches",
  templateUrl: "./repository-branches.component.html",
  styleUrls: ["./repository-branches.component.css"]
})
export class RepositoryBranchesComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private userRepositoriesManagerService: UserRepositoriesManagerService,
    private location: Location
  ) {}

  ngOnInit() {
    this.getBranches();
  }

  getBranches() {
    this.owner = this.route.snapshot.paramMap.get("owner");
    this.repo = this.route.snapshot.paramMap.get("repo");
    this.userRepositoriesManagerService
      .getRepoBranches(this.owner, this.repo)
      .subscribe(branches => {
        console.log("branches :", branches);
        this.branches = branches;
      });
  }

  goBack(): void {
    this.location.back();
  }

  onSelectCommits(sha: string) {
    this.userRepositoriesManagerService
      .getRepoCommits(this.owner, this.repo, sha)
      .subscribe(commits => {
        console.log("commits :", commits);
        this.selectedCommits = commits;
      });
  }

  branches: Branch[];
  owner: string;
  repo: string;

  selectedCommits: Commit[];
}
