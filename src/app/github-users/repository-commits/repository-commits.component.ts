import { Component, OnInit, Input } from "@angular/core";
import { Commit } from "../commit";
import { UserRepositoriesManagerService } from "../user-repositories-manager.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-repository-commits",
  templateUrl: "./repository-commits.component.html",
  styleUrls: ["./repository-commits.component.css"]
})
export class RepositoryCommitsComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private userRepositoriesManagerService: UserRepositoriesManagerService
  ) {}

  ngOnInit() {
    // this.getCommits();
  }

  getCommits() {
    const owner = this.route.snapshot.paramMap.get("owner");
    const repo = this.route.snapshot.paramMap.get("repo");
    const sha = this.route.snapshot.paramMap.get("sha") || null;
    this.userRepositoriesManagerService
      .getRepoCommits(owner, repo, sha)
      .subscribe(commits => {
        console.log("commits :", commits);
        this.commits = commits;
      });
  }

  filterByDate(commits) {
    return commits.filter(commit => {
      const commitDate = new Date(commit.commit.author.date);
      if (this.dateFrom && this.dateFrom > commitDate) {
        return false;
      }
      if (this.dateTo && commitDate > this.dateTo) {
        return false;
      }
      return true;
    });
  }

  onDateChangeFrom(event): void {
    this.dateFrom = event.value;
  }

  onDateChangeTo(event): void {
    this.dateTo = event.value;
  }

  // dateFrom:
  @Input() dateFrom: Date;
  @Input() dateTo: Date;
  @Input() commits: Commit[];
}
