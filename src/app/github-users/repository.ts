export class Repository {
  id: number;
  name: string;
  private: boolean;
  owner: {
    login: string;
  };
  description: string;
  fork: boolean;
  created_at: string;
  updated_at: string;
  pushed_at: string;
  homepage: string;
  size: number;
  stargazers_count: number;
  watchers_count: number;
  language: string;
  forks_count: number;
  mirror_url: string;
  archived: boolean;
  disabled: boolean;
  open_issues_count: number;
  license: string;
  forks: number;
  open_issues: number;
  watchers: number;
  default_branch: string;
}
