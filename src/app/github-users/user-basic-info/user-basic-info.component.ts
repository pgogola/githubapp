import { Component, OnInit, Input } from "@angular/core";
import { User } from "../user";

@Component({
  selector: "app-user-basic-info",
  templateUrl: "./user-basic-info.component.html",
  styleUrls: ["./user-basic-info.component.css"]
})
export class UserBasicInfoComponent implements OnInit {
  @Input() user: User;

  constructor() {}

  ngOnInit() {}
  getYear(date: string): string {
    if (!date) {
      return undefined;
    }
    const d: Date = new Date(date);
    return `${("0" + d.getDate()).slice(-2)}.${("0" + (d.getMonth() + 1)).slice(
      -2
    )}.${d.getFullYear()}`;
  }
}
