import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserRepositoriesInfoComponent } from './user-repositories-info.component';

describe('UserRepositoriesInfoComponent', () => {
  let component: UserRepositoriesInfoComponent;
  let fixture: ComponentFixture<UserRepositoriesInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserRepositoriesInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserRepositoriesInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
