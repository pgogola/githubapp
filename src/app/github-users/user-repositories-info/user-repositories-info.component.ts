import { Component, OnInit, Input } from "@angular/core";
import { Repository } from "../repository";
const copy = require("copy-to-clipboard");

@Component({
  selector: "app-user-repositories-info",
  templateUrl: "./user-repositories-info.component.html",
  styleUrls: ["./user-repositories-info.component.css"]
})
export class UserRepositoriesInfoComponent implements OnInit {
  @Input() repositories: Repository[];

  constructor() {}

  ngOnInit() {}

  getYear(date: string): string {
    if (!date) {
      return undefined;
    }
    const d: Date = new Date(date);
    return `${("0" + d.getHours()).slice(-2)}:${("0" + d.getMinutes()).slice(
      -2
    )}:${("0" + d.getSeconds()).slice(-2)} ${("0" + d.getDate()).slice(-2)}.${(
      "0" +
      (d.getMonth() + 1)
    ).slice(-2)}.${d.getFullYear()}`;
  }

  copyToClipboard(element) {
    copy(element);
  }
}
