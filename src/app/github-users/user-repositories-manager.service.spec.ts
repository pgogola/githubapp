import { TestBed } from "@angular/core/testing";

import { UserRepositoriesManagerService } from "./user-repositories-manager.service";

describe("UserRepositoriesManagerService", () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it("should be created", () => {
    const service: UserRepositoriesManagerService = TestBed.get(
      UserRepositoriesManagerService
    );
    expect(service).toBeTruthy();
  });
});
