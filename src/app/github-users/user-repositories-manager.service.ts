import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";
import { User } from "./user";
import { Repository } from "./repository";
import { Commit } from "./commit";
import { Branch } from "./branch";
import { GithubData } from "../environments/github_data";
@Injectable({
  providedIn: "root"
})
export class UserRepositoriesManagerService {
  constructor(private httpClient: HttpClient) {}

  MAIN_URL = "https://api.github.com";
  USERS_URL = `${this.MAIN_URL}/users`;
  REPO_URL = `${this.MAIN_URL}/repos`;
  private urls = {
    getUserHtmlUrl: username => {
      return `https://github.com/${username}`;
    },
    getSingleUserUrl: username => {
      return `${this.USERS_URL}/${username}`;
    },
    getUsersReposUrl: username => {
      return `${this.USERS_URL}/${username}/repos`;
    },
    getUserSubscriptionsUrl: username => {
      return `${this.USERS_URL}/${username}/subscriptions`;
    },
    getUserOrganizationsUrl: username => {
      return `${this.USERS_URL}/${username}/orgs`;
    },
    getUserFollowersUrl: username => {
      return `${this.USERS_URL}/${username}/followers`;
    },
    getRepoCommitsUrl: (owner, repo, sha = null) => {
      return `${this.REPO_URL}/${owner}/${repo}/commits${
        sha ? "?sha=" + sha : ""
      }`;
    },
    getRepoBranchesUrl: (owner, repo) => {
      return `${this.REPO_URL}/${owner}/${repo}/branches`;
    }
  };

  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      // this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  private httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
      Authorization:
        "Basic " + btoa(`${GithubData.LOGIN}:${GithubData.PASSWORD}`)
    })
  };

  getSingleUser(username): Observable<User> {
    const url = this.urls.getSingleUserUrl(username);
    return this.httpClient
      .get<User>(url, this.httpOptions)
      .pipe(catchError(this.handleError<User>("getSingleUser")));
  }

  getUserRepos(username): Observable<Repository[]> {
    const url = this.urls.getUsersReposUrl(username);
    return this.httpClient
      .get<Repository[]>(url, this.httpOptions)
      .pipe(
        catchError(this.handleError<Repository[]>("getUserRepositories", []))
      );
  }

  getUserSubscriptions(username): Observable<Object> {
    const url = this.urls.getUserSubscriptionsUrl(username);
    return this.httpClient
      .get<Object>(url, this.httpOptions)
      .pipe(catchError(this.handleError<Object>("getUserSubscriptions", {})));
  }

  getUserOrganizations(username): Observable<Object> {
    const url = this.urls.getUserOrganizationsUrl(username);
    return this.httpClient
      .get<Object>(url, this.httpOptions)
      .pipe(catchError(this.handleError<Object>("getUserOrganizations", {})));
  }

  getUserFollowers(username): Observable<Object> {
    const url = this.urls.getUserFollowersUrl(username);
    return this.httpClient
      .get<Object>(url, this.httpOptions)
      .pipe(catchError(this.handleError<Object>("getUserFollowers", {})));
  }

  getRepoCommits(owner, repo, sha = null): Observable<Commit[]> {
    const url = this.urls.getRepoCommitsUrl(owner, repo, sha);
    return this.httpClient
      .get<Commit[]>(url, this.httpOptions)
      .pipe(catchError(this.handleError<Commit[]>("getRepoCommits", [])));
  }

  getRepoBranches(owner, repo): Observable<Branch[]> {
    const url = this.urls.getRepoBranchesUrl(owner, repo);
    return this.httpClient
      .get<Branch[]>(url, this.httpOptions)
      .pipe(catchError(this.handleError<Branch[]>("getRepoBranches", [])));
  }
}
