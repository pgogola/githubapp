import { Component, OnInit } from "@angular/core";
import { UserRepositoriesManagerService } from "../user-repositories-manager.service";
import { User } from "../user";
import { Repository } from "../repository";
@Component({
  selector: "app-user-repositories",
  templateUrl: "./user-repositories.component.html",
  styleUrls: ["./user-repositories.component.css"]
})
export class UserRepositoriesComponent implements OnInit {
  constructor(
    private userRepositoriesManagerService: UserRepositoriesManagerService
  ) {}

  ngOnInit() {}

  getSingleUser(username) {
    this.userRepositoriesManagerService
      .getSingleUser(username)
      .subscribe(user => {
        this.fetchedUser = user;
      });
  }

  getUserRepos(username) {
    this.userRepositoriesManagerService
      .getUserRepos(username)
      .subscribe(repositories => {
        this.fetchedRepositories = repositories;
        console.log("repositories :", this.fetchedRepositories);
      });
  }

  getUserGithub(username) {
    this.getSingleUser(username);
    this.getUserRepos(username);
  }

  fetchedRepositories: Repository[];
  fetchedUser: User;
}
