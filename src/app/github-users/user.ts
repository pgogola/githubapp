export class User {
  login: string;
  id: number;
  avatar_url: string;
  bio: string;
  created_at: string;
  updated_at: string;
  followers: number;
  following: number;
  type: string;
  name: string;
  company: string;
  blog: string;
  location: string;
  email: string;
  public_repos: number;
  public_gists: number;
  total_private_repos: number;
  owned_private_repos: number;
  private_gists: number;
  disk_usage: number;
  colaborators: number;
}
